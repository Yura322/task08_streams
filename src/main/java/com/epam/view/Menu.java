package com.epam.view;

import com.epam.controller.TaskController;
import com.epam.controller.TaskControllerImpl;
import com.epam.model.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private Map<String, Command> commandMap;
    private Map<String, String> menu;
    private TaskController mainController = new TaskControllerImpl();
    private static Logger log = LogManager.getLogger(Menu.class);
    Command command = new Command() {
        @Override
        public void execute() {
            mainController.runThirdTask();
        }
    };

    public Menu() {
        initMenu();
        commandMap = new HashMap<>();
        commandMap.put("menu", this::showMenu);
        commandMap.put("task1", () -> mainController.runFirstTask());
        commandMap.put("task3", command);
        commandMap.put("task4", new Command() {
            @Override
            public void execute() {
                mainController.runFourthTask();
            }
        });
    }

    public void show() {
        Scanner scanner = new Scanner(System.in);
        String flagExit;
        log.info("input 'menu' to show Menu");
        do {
            flagExit = scanner.next();
            try {
                commandMap.get(flagExit).execute();
            } catch (NullPointerException e) {
                log.info("Input correct option\n");
            }
        } while (!flagExit.equals("exit"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("task1", "Show Result of 1 task");
        menu.put("task3", "Show Result of 3 task");
        menu.put("task4", "Show result of 4 task");
        menu.put("exit", "Exit");
    }

    private void showMenu() {
        menu.values().forEach(s -> log.info(s));
        log.info("Inputs: ");
        menu.keySet().forEach(k -> System.out.print(k + " "));
    }
}
