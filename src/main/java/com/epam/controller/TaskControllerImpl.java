package com.epam.controller;

import com.epam.model.CustomFunctionalInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TaskControllerImpl implements TaskController {

    private static Logger log = LogManager.getLogger(TaskControllerImpl.class);
    private List<String> inputList = new ArrayList<>();

    @Override
    public void runFirstTask() {
        CustomFunctionalInterface maxValue = (a, b, c) -> Stream.of(a, b, c).max(Comparator.naturalOrder()).get();
        CustomFunctionalInterface average = (a, b, c) -> (a + b + c) / 3;
        log.info("values = 3 ,2 ,5");
        log.info("maxValue = " + maxValue.apply(3, 2, 5));
        log.info("average = " + average.apply(3, 2, 5) + "\n");
    }

    @Override
    public void runThirdTask() {
        List<Integer> randomList = randomListGenerator();
        IntSummaryStatistics stat = randomList.stream().mapToInt(s -> s).summaryStatistics();
        int reduceStream = randomList.stream().reduce((i1, i2) -> i1 + i2).orElse(0);
        IntStream sumStream = randomList.stream().mapToInt(i -> i);
        double avarage = randomList.stream().mapToInt(i -> i).average().orElse(0);
        long biggerAvarageStream = randomList.stream().filter(i -> i > avarage).count();
        log.info("Summary statistic : " + stat);
        log.info("Sum reduce : " + reduceStream);
        log.info("Sum stream : " + sumStream.sum());
        log.info("Bigger than avarage count : " + biggerAvarageStream);
    }

    @Override
    public void runFourthTask() {
        log.info("Please input your text");
        readInput();
        String[] wordsArray = inputList.stream()
                .flatMap((p) -> Arrays.stream(p.split(" ")))
                .map(p -> p.replaceAll("\\W", ""))
                .toArray(String[]::new);
        sortUniqueWords(wordsArray).forEach(s -> log.info(s));
        log.info("Number of unique words : " + Arrays.stream(wordsArray).distinct().count());
        log.info("Word Count : " + countOccurrenceWord(wordsArray));
        log.info("Symbol Count : " + countOccurrenceEachSymbol());

    }

    private List<String> sortUniqueWords(String[] wordsArray) {
        List<String> sortedUniqueWords;
        sortedUniqueWords = Arrays.stream(wordsArray)
                .sorted()
                .collect(Collectors.toList());
        return sortedUniqueWords;
    }

    private Map<String, Long> countOccurrenceWord(String[] wordsArray) {
        Map<String, Long> wordOccurrenceCount;
        wordOccurrenceCount = Arrays.stream(wordsArray).collect(
                Collectors.groupingBy(s -> s, Collectors.counting()));
        return wordOccurrenceCount;
    }

    private Map<Character, Long> countOccurrenceEachSymbol() {
        Map<Character, Long> symbolCount;
        List<Character> charArray = inputList.stream()
                .reduce((s1, s2) -> s1 + s2)
                .orElse("")
                .chars()
                .mapToObj(c -> (char) c)
                .filter(Character::isLowerCase)
                .collect(Collectors.toList());
        symbolCount = charArray.stream().collect(
                Collectors.groupingBy(c -> c, Collectors.counting()));
        return symbolCount;
    }

    private List<Integer> randomListGenerator() {
        List<Integer> integerList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            integerList.add(random.nextInt(50));
        }
        return integerList;
    }

    private void readInput() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String nextLine = scanner.nextLine();
            if (nextLine.equals("")) {
                break;
            }
            inputList.add(nextLine);
        }
    }
}
