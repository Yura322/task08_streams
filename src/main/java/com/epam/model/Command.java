package com.epam.model;

@FunctionalInterface
public interface Command {
    void execute();
}
