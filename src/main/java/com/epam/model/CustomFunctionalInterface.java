package com.epam.model;

@FunctionalInterface
public interface CustomFunctionalInterface {
    int apply(int a , int b , int c);
}
